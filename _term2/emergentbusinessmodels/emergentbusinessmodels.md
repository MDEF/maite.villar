---
title: EMERGENT BUSINESS MODELS
period: April 2019
date: 2019-04-20 12:00:00
layout: page
term: 1
published: true
---

A sustainability model can help a project define a method of action that can provide long-term viability. Feasibility can be a compensation of internal efforts for benefits, which can be both economic and strategic collaborations.

A traditional business model is usually made up of costs and revenues. Revenues define (i) the segment you are targeting, (ii) the target, (iii) the channel, (iv) the value proposition and (v) revenues. On the other hand, costs are composed of (vi) key resources, (vii) key activities, (viii) key collaborations and (ix) cost structure.

At this stage of the project there is no offer generated so designing a business model for something abstract is somewhat difficult. Once the master is finished, it is probable that we will need to make a model in case we want to exhibit the installation in different events.

With a view to being able to design a service with sufficient solidity to bet on it, the information gathered during the classes of “Emerging business models” is shown below in order to gather the logic based on minimizing efforts and increasing impact.

The classes were given by “Javi Creus” and “Valeria Righi” workers of the company “Ideas for change”. Javi Creus is considered one of the most innovative strategists and thinkers on collaborative economics, open and P2P business models, citizen innovation and network society.  Valeria righi, researcher of ideas for change, is the author of 20 academic publications on topics related to participatory design with and for communities.







---

Next some of the presentation’s question guides and project related results.

Questions to minimize (internal & external) effort:
- What do you want to change?
- How do you want to change?
- What’s the variable from where we are going to measure we are successful or not.

- To minimize internal efforts
- Facilitate


**Minimize efforts, maximize impact**

---

**1) Define your impact variable**

Define your idea in 1 tweet:
- Design spaces to explore beyond what one already knows. New experiences will be developed to open perspectives
and look at different points of view and therefore to dialogue and reflect about our perceptions.

- Which system/s does your innovation belong to?
Social relations system.

- What variable of the system do you want to change/impact?
Encounter with people different from the one. // Number of synergies - increasing number of  people out from one’s field

**2) Analyse previous innovations**

- Shared and public services: From Roman baths, to public spaces, squares, markets, schools.
- Accessible planes: Option to travel (democratization)
- Mobile phone and access to information (democratization)
- Social media for social relationships: Facebook / Tinder / meetup

**How had previous innovation challenged the impact variable in analysis? How does your innovation?**

![]({{site.baseurl}}/pentagrowth.png)


**3) Refine your idea**

- What is your main impact variables?  
Aumentar las interacciones y lidiar con situaciones y gente distinta a uno. Número de interacciones con nueva gente - resultado: sinergias/colaboraciones.

- What is the most essential element of your innovation?
The physical encounter with things/people/situations that are not familiar to one and helps to have a wider view.

- What key elements have previous innovations changed? How this influences your idea?
The medium to connect with new relations. the “modus operandi”. The space to meet people, from the space to the digital, and from the private to public.

- What can I learn from previous innovations?
The innovations I’ve detected generated impact when being open or making them more accessible to everybody. “This innovation changes the experiences people had affecting on how they build the perceptions and relates to the world.”(Subjective opinion)

- How can I minimize internal efforts and maximize adoptions, by focusing on the most important features of my product that generates impact?
Gathering the existing information of actual organization and associations and designing an infrastructure for methods that helps sinergies happen.


**Systemic innovation. Innovation is recombination.**

---

The introduction for systemic innovation was an explanation oh how the recombination makes something innovative. For example, if there are elements, rules and agents in a food recipe the elements would be the ingredients, the rules the processes or recipes and the agents the chef’s or people involved. The less ingredients there are, the less recipe opportunities would be.

With this example they introduced different examples of how technology has the potential to combine the elements: NEST, uses real time data to manage the termostat; Google Sunroof generates data abundance from the information from maps, combining the squarefit of a sunroof m2 and sunlight hours. After they facilitate the connection with solar panel companies; City planner uses the open data public transit system and user movement to redesign the cities movilities.

Appart of the elements and the rules, the agents has importance when looking at the system.  The agent has transformed from being a consumer to have different roles in order to the services.

![]({{site.baseurl}}/theparticipationscale.jpg)

This graph shows that there is a transition to a new production model that can be identified in companies such as AIRBNB, where citizens are able to generate abundance where there was previously scarcity.

To finish gathering the inputs we have received in “Emerging business models” class here is the link to the “Pentagrowth” model that is the base of almost all the information they gave us: [Link to the webpage](https://www.ideasforchange.com/pentagrowth)

Shortly, this is a model that shows the 5 levers for accelerated growth: **Connect, Collect, Empower, Enable and Share.**

Notes:

1.  **Connect /NETWORK:** social-Mobile-things
What’s gonna be the next network change? We need to be situated there. That’s what is gonna drive the next way of growth. / Example: IOT

2. **Collect /INVENTORY:** Centralized-Distributed-Commons
How is going to be the offering to your users? / Example: BLABLACAR

3. **Empower /USERS:** Users-users/producers-any role
How many roles of your users are you able to integrate in your service? How I do for the service not to rely in me? Provide the conditions for the people to make the interactions and capture a fracture of the value of those interactions.

4. **Enable /PARTNERS:** Provide-Co-markets-Co-create
You can build your business on top of my business. I provide the tools and take a percentage. Competition is amoun ecosystem and standards no businesses. / Ex: NEST (Let’s be compatible)

5. **Share /KNOWLEDGE:** Proprietary-Non-commercial-Open
Review the knowledge you have an which part you take as proprietary and which part you share in which conditions. / WIKIPEDIA. **Sharing is a different management.**

**For the personal application the questions one could follow are these:

On which external resources can you leverage to minimize efforts and maximize probability?

- Connect: What network do you design for?
- Collect: How can we add new units of value and build your available supply reducing internal effort?
- Empower: What capacities of your users could you integrate in your organization / production processes?
- Enable: What tools can you provide to third parties so they can generate value?
- Share: What unused content does the company have?**
