---
title: ATLAS OF WEAK SIGNALS
period: April 2019
date: 2019-04-20 12:00:00
layout: page
term: 1
published: true
---

The syllabus for this course is the next: “Every future scenario is built by weak signals that set trends and point to certain directions, based on the analysis of the main change factors we can detect in the present. And the present for 2019 is a convulsed place, subjected to immense systemic crises that generate doubts about the survival of the status quo in multiple spheres. Any cartography we use for understanding the present requires an analysis of the main crises that determine our collective future,”

The course was divided into *“Atlas of weak signals, definitions”* given by *Jose Luis de Vicente* and *Mariana Quintero*, with the aim of pointing out the trends that shows a big picture of the systemic challenges we are facing nowadays. On the other hand *Ramon Sanguesa* and *Lucas Peñas* in *“Atlas of weak signals, development”* have taught us the process of making a repository to collect those trends and visualize them by data scraping.

The trends we talked about are:

Life in the times of Surveillance:
- Capitalism  
- Attention protection  
- Dismantling ﬁlter bubbles
- Circular data economy
- Truth Wars  
- Redesigning social

Designing for the Anthropocene  
- Climate conscience
- Inter-species collaboration  
- Long-termism  
- Carbon neutral lifestyles  
- Fighting Anthropocene conﬂicts

Life after AI - the end of work
- Technology for equality  
- Fighting AI bias  
- Imagining new jobs  
- Making Universal Basic Income work  
- Human-machine creative collaborations

After the nation-state  
- Making world governance  
- Rural futures  
- Pick your own passport
- Refugee tech  
- Welfare state 2.0

Kill the heteropatriarchy  
- Non-heteropatriarchal innovation  
- Imagining futures that are not western-centric  
- Reconﬁgure your body  
- Gender ﬂuidity
- Disrupt ageism

During the weeks we have work in different groups in the most of the topics are shown before. We feed a shared excel organizing each window with a topic and completed with links and keywords related to the references. This is the sheet that we are using for data scraping. Saira Raza made a map of the weak signals with the keywords and the topic we have been working on. Here the result: [INTERACTIVE MAP](https://kumu.io/sraza/materials6#aowsscraped)

![]({{site.baseurl}}/resources/weaksignalsmap.png)

An example to understand a weak signal:

![]({{site.baseurl}}/resources/sonarcall.jpg)

The experiment done in Sonar 2018. They sent a radio message exploring the opportunity to have a response in 25 year time.

“Sónar Calling GJ273b” is the first radio message ever to be sent to a nearby potentially habitable exoplanet. The message includes 33 music pieces of 10 seconds each, commissioned exclusively for Sónar from artists who encapsulate Sónar's exploratory approach to music over its quarter century of existence. Any possible response could arrive in 25 years time, coinciding with Sónar's 50th anniversary.”

*I have written in my notebook:*
##A weak signal is to hear the sound of the birds that usually you don’t use to listen.

How to identify a weak signal?

----

Managing weak signals (Steps)
- Scan
- Isolate
- Evaluate
- Organize
- Communicate

Scanning for weak signals
- Conversations
- Persistent secondary topics / Controversial items
- How to look in conversations - There are topics
- Dynamics of conversation /Contropedia
- Data
- Anomalies
- Event
- Impact

Techniques
- Text mining
- Anomaly detection in data
- Network analysis
- Trend Analysis
- Temporal Data analysis
- Controversy Mapping

Organizing
- Ontology mapping
- Wiki database
- Knowledge federation

Connecting to the project

---

The main issue that concerned most of my academical decision, more or less since I was 16 years old is that alternatives must be an option. We are part of a system but we can do things differently although you are still on it.

The next map is a mapping of some concepts that affects into the polarization of society. That most of the trends that have been presented seems are boosting through this direction:

![]({{site.baseurl}}/resources/mindmap.jpg)

In relation to this broad topic, I have focused my intervention on the area of social relations. I think that social relations, in the physical world, play a key role in the well-being of a society. It seems that the connection between people has changed definition since the introduction of digital in the world. How does this affect our relationships? Why is it important?

The objective answers are the ones I want to investigate through intervention; with respect to the subjective answer I can say that I firmly believe that the encounter with the unknown is necessary to develop skills such as empathy, dialogue or the construction of an opinion. These skills seem to me to be indispensable for the world in which we live and the world that seems to be coming.

This is why I want to study how social relations are affected by suggestion algorithms and their effect.

In relation to the intervention I signal some of the next trends that are interesting to follow the research:

- **Capitalism:**
The core of all my decisions in the academical world is to understand how we can organize in alternative ways. It's why I'm so interested in oganizational transformation. The main reason I do what I do is becouse time ago I undertood this system was inhumman the way it have prevaleced since today. Luckly a lot of people achieve to make things differently inside this system, and there is where I look at. A transversal area of research. We need to transform our society from an ego centered system to an **eco-system.**

- **Non-heteropatriarchal innovation**
For this system transformation we need to change the structures that drives this society. It seems that we have traduced the same structures to the digital. A system where the forces are distributed by power, and the ones with power are still the white men that are driven by the factic power. The other side of the coin shows the potentiality of the tools, knowledge, people and a long etc that are aviable in the world that have already change the world and still is a lot to do.

To design for eco-systems we need to think in innovation as a recombination of existing elements to change the status quo.

- **Redesigning social media** and **Dismantling ﬁlter bubbles**
Some of the hetero-heteropatriarchal innovative companies ,subjectivly, are Facebook or Instagram. the huge companies that are using the personal data as the main active for advertaisment are based in suggestion based algorithms. They target you better, gathering the data, the most you use the app so the content and the adverts are more an more personalized to the user.

Some experts are talking about the possible effect of these personalization based algorithms. When everything you read in a screen is adapted to you and everything you don't want to see can be deleted with a click, a big source of information that defines your world is reafirming continuosly your own beliefs.

This reafirmation of the beliefs generates a "Reality bubble" which doesn't let the people on it look out of it.

**Is this helping us to turn our backs on the real situations that are happening in the world?**

## Is this effect boosting a disconnection with the reality by the hyperconnection?

- **Fighting Anthropocene conﬂicts**
We need to organize differently yo face the conflicts that are coming.
