---
title: 03. DESIGN FOR THE REAL DIGITAL WORLD
period: 15-21 October 2018
date: 2018-10-21 12:00:00
layout: page
term: 1
published: true
---

![]({{site.baseurl}}/W3_Design.jpg)
![]({{site.baseurl}}/W3_Design2.jpg)
![]({{site.baseurl}}/W3_Design3.jpg)
![]({{site.baseurl}}/W3_Design4.jpg)
![]({{site.baseurl}}/W3_Design5.jpg)
![]({{site.baseurl}}/W3_Design6.jpg)
![]({{site.baseurl}}/W3_Design7.jpg)
![]({{site.baseurl}}/W3_Design8.jpg)
![]({{site.baseurl}}/W3_Design10.jpg)
