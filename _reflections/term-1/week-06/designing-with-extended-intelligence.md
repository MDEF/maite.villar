---
title: 06. DESIGN WITH EXTENDED INTELLIGENCE
period: 5-11 November 2018
date: 2018-11-11 12:00:00
layout: page
term: 1
published: true
---

![]({{site.baseurl}}/W6_Extendedinteligence.jpg)
![]({{site.baseurl}}/W6_Extendedinteligence2.jpg)
![]({{site.baseurl}}/W6_Extendedinteligence3.jpg)
![]({{site.baseurl}}/W6_Extendedinteligence4.jpg)
![]({{site.baseurl}}/W6_Extendedinteligence5.jpg)
![]({{site.baseurl}}/W6_Extendedinteligence6.jpg)
