---
title: 12. OUTPUT DEVICES
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---


The assignment of week 11 has been to mill a pcb with an output component, solder and program it.

I have choosen the "RGB" light with the aim to connect in "Network and communication" the input (temperature sensor) and out board.
![]({{site.baseurl}}/helloRGB_info.png)

The files to download are accesible in the Fab Academy page:
- [**Output devices: RGB led (Click here)**](http://academy.cba.mit.edu/classes/output_devices/index.html)

I've downloaded the **traces**, **interior** and the **components file**:
![]({{site.baseurl}}/w12_board.jpg)

The process of milling the board is described in my week 5 *Electronic production* documentation:
- [**Click here to access to the documentation of "milling the board".**](https://mdef.gitlab.io/maite.villar/fabacademy/electronicsproduction/)

Or more clearly I use to follow this super detailed tutorial for the *SRM20 milling machine*:
- [**For a more detailed tutorial I use this one. Click here.**](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)

![]({{site.baseurl}}/board_one.png)
![]({{site.baseurl}}/board_two.png)

Once the board has been cutted I took the components needed and solder it after:
![]({{site.baseurl}}/outputdevices_rgb_wrongcomponent.jpeg)

This process starts to be quite easy.

**Programming the board**

---

**Finding any shortcuts before programming**

I used the multimeter to detect if there was any shortcut. With the schematic in a side a tried all the combinations possible and for the firs time it seems there was no shortcut !

I've connected the tinny US to the ISPboard we made in the week 5 and program it to use it as programmer. I've connected the ISPboard to this new board and open the Arduino software.

In instance I found there was a failure. One component was wrong.
![]({{site.baseurl}}/rgb_wrongcomponent.jpeg)

After asking about this mistake I have learnt that that component is for "canalizing" power. As the RGB light consumes a lot of energy but the board is designed for 5 Volts, there is the posibility to add a component that from an external power canalizes the voltages for sending 5V to the RGB led. As I don't need it, I quit it and program it.

**Programming in arduino**

I've opened Arduino One software and connect the ISPboard and the board.

In the tool window of the software I put the next:

- Change when it says **Serial** to **SoftwareSerial**
- In the window "tools"
- Board: Attiny45
- Processor: Attiny45
- Clock: 1Hz internal. (It's not using any clock so it's not needed to change)
- Programmer: ISPboard
- **Burn bootloader** to see if the board works.

Everything was correct so I've added the **"Blink"** example from basic examples that the software has.

To write the pins of the RBG led I look into the Attiny45 datasheet and the schematic of my board.
![]({{site.baseurl}}/input_pinout.jpg)

- Red (R) - PB1 - *Arduino pin:* **2**
- Blue (B) - PB2 - *Arduino pin:* **1**
- Green (G) -  PB0 - *Arduino pin:* **0**

![]({{site.baseurl}}/capture_MAITE1.JPG)

In the **void setup** I've changed:

pinMode (0, Output);
pinMode (1, Output);
pinMode (2, Output);

![]({{site.baseurl}}/capture_MAITE.JPG)

An swith it with the example code (Chaning the Pin numer). In the first result the colour and the time was not the one I wanted.
![]({{site.baseurl}}/rgb_result1.gif)

The capacitor next to the RGB led was changing how the light blinks. In this case for lighting in the colour I wanted I needed to change the code writting **HIGH** in the colours I wanted to switch off and **LOW** in the one I wanted to switch ON. I've added a delay after making *Blink* in a colour to have the three colour blinking one after the other.

 It worked. Here the result:

 ![]({{site.baseurl}}/rgb_finalresult.gif)

 **Download the files**

 ---

 - Board files [Link to the download the files]({{site.baseurl}}/RGB.boardfiles_png.zip)
