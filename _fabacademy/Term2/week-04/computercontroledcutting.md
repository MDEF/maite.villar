---
title: 04. COMPUTER CONTROLED CUTTING
period: 06-13 February 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---


## Machine set up
-----

We did the "Kerf test" at the FabLab in Barcelona. The machine used for this exercise is specifically **“**[**Speedy 400**](https://www.troteclaser.com/en/laser-machines/laser-engravers-speedy-series/)**”** by[ **“Trotec**](https://www.troteclaser.com/)**”**.

![]({{site.baseurl}}/w4_image1.jpg)

The characteristics of this machine are:

Work area - **1000 x 610 mm**  
Max. Workpiece height - **296 mm**
Laser power - CO2 - **40 - 120 watts**  
Laser power - Fiber - **10 - 50 watts**
Flexx technology -  **Included**

If you are a fan of laser cutters these features will give you an idea of the power of the machine. For beginners I have collected the details to meet the safety conditions and understand the process of preparing the file, material and its execution.

First, prepare the files:

The formats this machine accepts are *.svg .dxf.* You can use programs such as *Illustrator, Inkscape, Fusion 360, Rhinoceros.* Make your design in one of those softwares and set up the machine.

**How to set up the machine?**

------

The buttons to control physically the machine are located on the right side of the machine.

![]({{site.baseurl}}/w4_image2.jpg)

To turn the machine on you will need to turn the on/off button. In the case of Barcelona you will have to ask who is in charge of the space where the machine is to turn it on and someone will supervise you.

To do it by your own there is a test where the basic requirements are asked to use the machines. Once you have passed this test it is not necessary to have supervision.

We turn on the machine and choose the material we are going to use. There are several options, the most accessible is our space are, methacrylate, Balsa, cardboard, Plywood, mdf and more.

The material that we use will condition the parameters that we introduce at the moment of preparing the file to send to cut.

Note the following conditions:

- The dimensions of the machine are 600x900mm.
- A "non-flat" material alters the effect of the laser, so the parameters do not affect the whole surface equally.
- To fix the material there use a tape.

Once the right size material is in hand the height of the machine must be adjusted.

![]({{site.baseurl}}/w4_image3.jpg)

With the white swab that is always in the right (after using it leave it in the same place) and the buttons that are indicated in the following image we adjust the height.

![]({{site.baseurl}}/w4_image4.jpg)

The stick has to be fixed to the laser so that the flat side is down and the stick is up. With the material in the tray we go up VERY slowly controlling by the buttons until the stick slightly hits the surface of the material and falls.

Then you have to prepare the files.



**Prepare the file to send to the machine**

------

Open the file in any of these formats .svg .dxf. In our case we have worked with a Rhino 5 file in the computer that it’s connected to the machine we want to use. *Note: In the Fab Lab of Barcelona there are two computers. Each one is connected to a different machine.*

This is the file we want to cut.

![]({{site.baseurl}}/w4_image5.png)

Before cutting the definitive file it’s necessary to do a test. This way there is no so much material waste and the trial can be repeated times since there’s a good result to achieve the objective you want with your file.

For that, we have done two squares, one for rastering and the other for cutting:

![]({{site.baseurl}}/w4_image6.png)

The squares are made using the next command. The first square is at point 0, the second I have clicked inside the area at an approximate point.

- Command: Rectangle
- First corner of rectangle ( 3Point  Vertical  Center  AroundCurve  Rounded ): 0
- Other corner or length ( 3Point  Rounded ) 10
- Other corner or length ( 3Point  Rounded ) 10

In order to distinguish the parts to engrave or to cut the most comfortable thing is to distinguish it by layers. In general to cut the default color is red and to engrave, black or blue.

To change the layer object, select the area you want to change and with the right mouse button click on the layer to which you want to change "Change object to layer".

![]({{site.baseurl}}/w4_image7.jpg)

Print it: **Control + p**

This window is going to appear: **Print setup**

![]({{site.baseurl}}/w4_image8.jpeg)

Important things to know:
\> Output type:
Vector output or Raster output
\> Output colour:
Display Color - It’s going to respect the colours established before.
\> View and Output scale:

Click **print** and the continuous window is going to be open:

Software: **TROTEC- JobControl X**. You can find super detailed information [here](https://www.troteclaser.com/en/knowledge/tips-for-laser-users/laser-parameter-basics-settings/**](https://www.troteclaser.com/en/knowledge/tips-for-laser-users/laser-parameter-basics-settings/)

PICTURE 1

*Notes*:

- *The square in the middle represents the table of the laser cutter.*
- *The blue square marks where the laser pointer is.*
- *The box on the left shows the information collected by the software.*
- *Each time you upload a file you have to change the parameters*


**Steps:**

1. **Delete job.** Delete the actual job

2. Arrastra desde la venta de la derecha el archivo que quieres hacer.

3. To see the file in the screen click into the **eye**

4. Settings window: Power / Speed / Hz

6. Pluma: Efficiency.

7. Before cutting **switch on the fan**.

Depending the result, repeat the process changing the parameter. Be careful not to make fire, it’s dangerous and it can break the laser glass. In case the fire start, push the big red button that seems a STOP.

If something is going wrong meanwhile the laser is working play with: Play, pause and stop.

We have followed this process each time we have used the lasercutter. We have made three different tests in a group (Kerf test, Press fit test and Rastering) and individually I experimented working in a parametric design to cut a "Press fit" thing.

**Press fit test**

-----

A [press-fit joint](https://en.wikipedia.org/wiki/Interference_fit) (also known as interference fit or friction fit) is a fastening between two parts which is achieved by friction after the parts are pushed together, rather than by any other means of fastening.

We found a piece of 6mm cardboard. We prepared the rhino file for the test with slots from 5.5mm to 6.5mm.

![]({{site.baseurl}}/w4_image9.png)

After printing the two parts we tested all the slots.

After printing the two parts we tested all the slots and our conclusions are:

- From 5.5mm to 6mm the joint is too tight and deforms the slot.
- 6.1mm is perfect, not too tight and not too loose.
- And from 6.2mm to 6.5mm it gets worst.

![]({{site.baseurl}}/w4_image9.1.jpg)
![]({{site.baseurl}}/w4_video1.mp4)

Test done on 11/02/19 with 6mm cardboard, cutting power of 50, speed of 1, 1000Hz and with the Speedy 400 machine.

**Kerf test**

-----

The kerf test is to check how much material is removed as the laser passes through a material. The gap caused by the laser is called the kerf. The thicker the material, the more slowly you have to pass through the material, and the wider the gap will be.
![]({{site.baseurl}}/w4_kerf.jpg)
In this test we put 9 pieces of wood next to each other with 10 lines. Each piece is 10mm wide. Once the parts were cut the pieces are put together and measured.
- The total should be 90mm
- The parts together measured 89.91mm
- The gap is 0.9mm, and if you divide it by the number of lines which is 10 it gives you kerf width of 0.09mm with this material and settings.

- Material - 4mm plywood
- Cutting power - 90
- Cutting speed - 1
- Cutting Hz - 1000
- Machine - Trotec Speedy400

Checking the kerf means that you can take into account the size when creating joints and make sure they will fit with the tolerance that you need.
![]({{site.baseurl}}/w4_kerf1.jpg)
![]({{site.baseurl}}/w4_kerf2.jpg)

**Engraving test**

-----

For the raster engraving test we started off with getting a bitmap image (of Alex’s cat!), and converting it to black and white. To do this I opened the image with Photoshop and set the image mode to grayscale by going to Image > Mode > Grayscale. I then edited the levels and curves on Photoshop to create more contrast.
![]({{site.baseurl}}/w4_engraving1.jpg)
This file was then imported into Rhino using the PictureFrame command to prepare the file for laser cutting.
![]({{site.baseurl}}/w4_engraving2.jpg)
Cutting, engraving, and rastering were separated into different layers. On the laser cutter, we used the Trotec Speedy 100R machine to test the parameters with our chosen material.
![]({{site.baseurl}}/w4_engraving3.jpg)
We used a piece of clear 3mm Acrylic scrap.
![]({{site.baseurl}}/w4_engraving4.jpg)
The initial parameters were successful upon the first cuts, and subsequently used for the following tests, the settings are as follows.

**Engrave**
- P: 70
- S: 100
- HZ: 20, 000
**Raster**
- P: 50
- S: 100
- HZ: 10, 000
**Cut**
- P: 70
- S: 0.5
- HZ: 20, 0000

After cutting, we realized that much more contrast was needed as our image did not show, therefore next time the image used will have clearer contours.
![]({{site.baseurl}}/w4_engraving5.jpg)

## Individual work
-----


This week the individual objective has been to design a parametric design and make a "press fit" structure.

I had two options; to work on a structure that was visually appealing through a normal 2D design, or make a simpler structure and work "grasshopper" in a more intuitive way until arriving at simple forms designed parametrically. The decision was to make the second one.

Here the process and the result:

The first thing has been to make a polygon to which I have put three corners to make a triangle.

![]({{site.baseurl}}/w4_image10.png)
P = Point, R = Radius, S = Segments, Rf = Fillet Radius.

In the following image I show the reference of each variable with respect to the geometry to make a triangle.
![]({{site.baseurl}}/w4_image11.jpg)

Once the triangle is done, the next step is to make the joints.
![]({{site.baseurl}}/w4_image12.png)
P = point, X = X size, Y = Y size, R = radius.

In the following image the reference of the variables with respect to the geometry, which this time is the rectangle.
![]({{site.baseurl}}/join2.jpg)

Continuously to "cut" the excess lines of the corners, I will first do *"Surfaces", "merge" and "Region difference".*
![]({{site.baseurl}}/w4_image13.jpg)

After this, I added "Array" function to add in Y or Z direction the quantity of geometries I desire.

The "C" is the reference for the center and it generates the distance between each figures. Adding a "Rectangle" I can manipulate it.
![]({{site.baseurl}}/w4_image14.png)
I leave it in red to for the laser cut software to understand it as the lines to CUT.

In the press fit the result we have seen is that for the cardboard the best measure is 6.1mm. So I will realize it this size.

Here the result:
![]({{site.baseurl}}/w4_individualwork.jpg)

**Vinyl cutter**

------

the machines used to vinyl is:  **Roland GS24 Camm-1**
![]({{site.baseurl}}/w4_machine.png)

- Working area : 580 x 1600 mm - because of the wheels that limit width
- Max force of the knife : 240gf
- Softwares : Inkscape (Roland printer driver) - CutStudio
- Tutorial on how to use the Roland GS24 - Vinyl cutter

I've not get crazy in the design, I wanted to do the exercise and make a sticker as a present to a friend. I writted in the CutStudio of the computer that is connected to the machine "Mari" (because her name ir Maria)

![]({{site.baseurl}}/w4_maria.jpg)

After I put the material into the machine. the machine tells you your workin area in its screen. The small pieces of the pictures are the ones fixing the material. Be sure that it's holded by the two, if not when cutting it's going to move and it's going to cut wrong.

![]({{site.baseurl}}/w4_materialtocut.png)

Cut a first test. A small square in a side and see if it worked well.

Back to CutStudio to send the job to the machine. Go to File / Cutting Setup, then Properties and use the Get from Machine button to get material dimensions and click OK.

Use the **pen force slider** if it's not cutting well.
Press Start:

![]({{site.baseurl}}/w3_cutting5.jpeg)

To separate the sticker you can use an tinny cutter to separate the negative or the positive part.

And this is my present for a smoker friend. 
![]({{site.baseurl}}/w3_cutting7.jpeg)

**Files**

------

- Kerf test file: Rhinoceros 6 [Link to the file]({{site.baseurl}}/resources/W4_KerftestR6.3dm)
- Engraving test file: Rhinoceros 6 [Link to the file]({{site.baseurl}}/W4_RasterengravingtestR6.3dm)
- Press fit individual work:
- Rhinoceros 5 *To use it in FabLab Barcelona* [Link to the file]({{site.baseurl}}/w4_parametricjoints.3dm)
- Grasshopper [Link to the file]({{site.baseurl}}/w5_parametricjoint.gh)
