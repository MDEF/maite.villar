---
title: 10. MOLDING AND CASTING
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

The assignment for this week was to mold and cast. we wanted to make something that could be usefull for making biscuits and give to people around. In this assignment [Adriana Tamargo](https://mdef.gitlab.io/adriana.tamargo/Molding.html) and me worked together.


The phases of the work development have been:
- Design appropiate files
- Molding and casting


**Design appropiate files**

----

This is the design we've done:
![]({{site.baseurl}}/molddesign.png)

We've only made the one that looks like a cone.

When doing the design we noticed that if the design was going to be cutted in soap with the milling machine it need to be designed as for the CNC machine. Taking in account that it needed to able to be cutted by 3 axes.

The design was going to be 3d printed so we could play a little bit more. In the picture shown before it's visible that the first object is the outcome of the casting. The mold is the positive part and it's the one that was 3d printed (and exported, Select the piece and export it):

![]({{site.baseurl}}/molddesign1.png)

When we designed it we take in account that it doesn't need to be really strong. It can be designed in a way that it easy to unmold taking in account the angles of the walls or easy to break. If you choose the second one it will be only a one use mold. In our case we weren't aware so we needed to break it.

We designed the mold higher than the part we wanted to cast in order to be able to cover it with the silicon. We realized after making the mold that in our case it was enough to have 5 milimeters to the height. Anyway it worked but we used more silicon than the needed.

**3d printing**
---

We opened the 3d file in Cura to print it in the Ultimaker machine.
We have adapted the parameters to the design. The process followed it's the same of the **3d printing** week. There are step by step tutorias avaiable in the **FabAcademy website**. (I will add the link Here)

In our case we changed the parameters of the support to print it faster. Changing one parameter change the time in hours.

Once the file is ready to print we've save it, put it in a pendrive and plug it to the **Ultimaker** Machine.

It's recommended to switch on the machine 5 minutes before to heat it up. So the fillament can melt easily from the beggining. Choose the file in the machine and wait.

When we had the mold we could start making the negative mold.

![]({{site.baseurl}}/w10_object.png)

**Workflow**

3d print the mold - Clean it - Dry it - Select the silicon you want to make the mold with - read the MSDS - security first - Aply the release agent - measure and mix - pouring, curing and molding.

**Molding and casting**

---

Now that we have the positive mold, we'll make the negative mold, the baking mold. For that, we'll use flexible silicone, since it's easy to release and has a good quality surface finish. We used the Sorta-Clear 40 Translucent Silicone Mold Rubber because of its food-safe quality.

![]({{site.baseurl}}/moldin1.png)

**Preparing the mold**

First, we need to clean the 3d mold. For that we cleaned it with water and then dry it. Then we added the release agent which is a silicon spray. Once cleaned we can start with the casting part. It's important to read the datasheet beforehand to make sure what type of release agent you need. In our case we didn't need it bat it said it could be used. So we used the release agent just in case, we used the Mann release agent 200, following the instructions of the container.

The silicon comes in a box with two substances. The MSDS it's used for knowing the safety practices to use the material. In the other there ir a two side paper where the product overview, the technical overview the processing recommendations are shown. We followed this second one to make the different steps of the process. [Click here to access online](https://www.smooth-on.com/tb/files/SORTA_CLEAR_SERIES_TB.pdfortant)

We looked for the **Mix ratio** of the SORTA clear 40, in this case it use 100A - 10B by weight. Once you know that, first is needes to calculate the volume of the mold with water (it's explained after how). one we know the volume of our mold we can calculate the exact amount of part A and part B that we need.  

The safety tips to take in account are: (for the Sorta clear -40)
- Use in a properly ventilated area (“room size” ventilation). Wear safety glasses, long sleeves and rubber gloves to minimize contamination risk. Wear vinyl gloves only. Latex gloves will inhibit the cure of the rubber. Store and use material at room temperature (73°F/23°C). Warmer temperatures will drastically reduce working time and cure time. Storing material at warmer temperatures will also reduce the usable shelf life of unused material. These products have a limited shelf life and should be used as soon as possible.

![]({{site.baseurl}}/molding2.png)
![]({{site.baseurl}}/molding3.png)

Followed steps:

1. Measure the volume of our mold. We did it with water, we fill in the mold with water and then put it in a glass, that’s the amount of volume of silicon we will need for this mold.

![]({{site.baseurl}}/molding4.png)

3. Measure the quantity of part A and part B we need, for that, we followed the mix ratio specified on the datasheet, calculating the exact quantity of each part that we needed for the volume of our mold. In this case we needed 15,8g of part A and 0,474 g of part B.
4. Mix part A + part B for 3 minutes, don’t stop mixing!
5. Vacuum the mix to take out the air, it was supposed to be beween 2-3 minutes, but we did it until we no longer could see bubbles.
6. Pour the mix in your mold very slowly and starting from the corners. After pouring all the mix we had, we realized it wasn’t enough and that we needed more mix, so we made more as fast as possible and filled in the part of the mold left.
7. Once the mold is filled in with the mix we need to vacuum it again.
8. Then we left the mix drying for 24h as the datasheet specified.

![]({{site.baseurl}}/molding5.png)

When we came back on Monday the silicon was ready but it was really difficult to take out the mold. That’s where we learned that is better to design the mold a bit opened in the upper side instead of a cube design, as you can see on the sketch below.
![]({{site.baseurl}}/molding6.png)

So we needed to break the rigid mold in order to take out the silicon mold…
![]({{site.baseurl}}/molding6.png)
![]({{site.baseurl}}/molding7.png)

..But we have a silicon mold for baking chocolate diamonds!

![]({{site.baseurl}}/molding8.png)
![]({{site.baseurl}}/molding9.png)

**And we are ready to cast the chocolate!**
The idea was to cast a chocolate piece with an strawberry inside, but we made the first triel just with chocolate. We found out that it was super complicated to unmold the chocolate because of the design of the mold. For the next time we should make the diamond more triangular so the top part is the wider one and is easy to uncast.

![]({{site.baseurl}}/castingpart.jpg)

**We have done one made by chocolate and another one made by ice. The next is made from local melted chocolate, we have put oil to make it easier to take it out, anyways we needed a knife to help taking it out.**
![]({{site.baseurl}}/castchoco.jpg)
**The next one is made by ice! It looks cool!**
![]({{site.baseurl}}/casthielo.jpg)

**files**

---

- mold for 3d printing [Link to the file]({{site.baseurl}}/moldfor3dprinting.stl)
