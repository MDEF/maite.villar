---
title: 15. APPLICATIONS AND IMPLICATIONS
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

**The scope of your project develop**

---

Da la sensación que las tendencias actuales como los servicios híper personalizados y por tanto enfocados en el interés individual están generando una mayor distancia entre las personas por un lado, y por otro en el desconocimiento del impacto y consecuencias de los mismos; siendo una de ellas el hecho de acomodarse en la propia zona de confort y en donde sólo hay espacio para lo conocido.) en el humano están generando mayor distancia entre las personas (por un lado, y en el desconocimiento de las relaciones (¿??) y sus consecuencias e impacto. en la sociedad y entre el conocimiento de la relación entre las acciones y su impacto,
en consecuencia de acomodarse en la zona de confort, donde solo hay espacio para lo conocido.

Para el desarrollo de este proyecto se han recogido varios estudios que muestran resultados sobre el efecto de la hiper personalización en relación con la confirmación de las ideas de uno mismo, con el objetivo de fundamentar la observación y desarrollar un trabajo que presente opciones para abordar esta problemática.

To know about the research behind the studio ask for the information: **maite.villar@iaac.net**


**what has done beforehand?**

---

Links to project references:

- **East western Divan** [Link to the site](https://www.west-eastern-divan.org/)
- **Max Hawking - Randomness** [Link to the site](https://maxhawkins.me/work/randomized_living.htm   )
- **White right** [Link to the site](https://www.vox.com/world/2019/1/14/18151799/extremism-white-supremacy-jihadism-deeyah-khan )
- **Graffiti receptes** [Link to the site](https://www.facebook.com/photo.php?fbid=10152869914648316&set=t.100008333919376&type=3&theater )
- **The machine to be another** [Link to the site](http://beanotherlab.org/ )
- **Connected humans** [Link to the site](http://connectedhumans.tools/ )
- **Notes to strangers** [Link to the site](https://selling-to-strangers.myshopify.com/)
- **Marina-abramovic** [Link to the site](https://extracine.com/2013/03/critica-de-marina-abramovic-the-artist-is-present)
- **Sergio Albiac** [Link to the site](https://www.sergioalbiac.com/ )


**Instalation proposal**

----

La intervención se presenta desde la base del encuentro con el otro. El objetivo es diseñar un espacio donde dos personas se encuentren y exista una interacción que provoque una reflexión del “yo” en el encuentro con el otro.

Trata de una instalación que requiere de una pantalla con una cámara, un proyector y una actividad para capturar las reflexiones. Tiene que ser entendible e intuitivo para que no requiera mi presencia.

Cuando uno acude a la instalación se encuentra con un espacio estilo “Fotomatón” con una pantalla donde se ve a sí mismo por medio de la cámara. (Paso 1/2) Pulsa un botón y se saca una foto; - Luz, Color 1 (Paso 2/2) aparece un texto que dice “para completar la foto, otra persona tiene que sacarse la foto” Luz, color 2; Una vez otro saque una foto - Apenas hay luz (Paso 3/3) La pantalla muestra un foto en doble exposición de las dos fotos sobreexpuestas. Ejemplo de una foto de doble exposición:

![]({{site.baseurl}}/doubleexposure.jpg)

Aparecen (5 segundos) hasta que se muestra la foto en la pantalla. 5 segundos hasta que se encienda la luz. En esos cinco segundos la foto se proyecta en un mosaico en la pared, donde se van recogiendo todas las composiciones que se han creado. La composición provoca una imagen de verse siendo “el otro”, para algunos perdiendo la identidad y para otras alimentando la identidad de uno junto con el otro”.

El mosaico junta a todas las combinaciones como representación de la diversidad y como reflejo de que somos parte de algo más grande que nosotros mismos.

La actividad aún no la sé: Posibles preguntas: Después de ver la foto..
¿Aún eres “tú”?

*Quiero hacer ver el valor de la sinergia. Los beneficios.
¿Es la conexión humana el antídoto para el efecto echo chamber en la sociedad?*

**The sketch-es of the instalation next:**

![]({{site.baseurl}}/sketch_1.jpeg)
![]({{site.baseurl}}/sketch_2.jpeg)
![]({{site.baseurl}}/sketch_3.jpeg)

**What materials and components will be required?where they come from?what it is the cost?**

----

Materials:
- Display
- Webcam
- Led strip (RGB)
- Wood (plywood)
- Capacitive Sensors
- arduino
- Rasperry Pi
- Power
- Projector
- Cables: USB(Display, webcam and Arduino to Rasperry Pi), Power(computer), Cable (Cap. sensor + led strip to arduino).

**what parts and systems will be made ? what processes used?**

---

There are going to be two boxes. They are going to be made by wood. I'm going to laser cut because they odn't need to be thick, the aim of using this box is to place the objects needed in a fixed box, so after I can adapt them to the space we have in the instalation.

I don't need to buil much thing. I need a black wall behind, but as we are receibing the structure made I'm going to add a thick black plastic.


**what taks need to be completed?**

1. Coding:
- Double exposure in processing and serial read of the capacitive sensor.
- Collage generator in processing to send to the projector.
- Rasperry Pi
2. Get the materials, mainly the display to design the box where it goes into.
3. Design and make the box.
4. Design the cables structure out of the box.

**what is the schedule? real vs ideal**

---

It's going to be real. For the 14th of June the instalation needs to be done.

**how will it be evaluated?**

---

With the mdef master grading system.
