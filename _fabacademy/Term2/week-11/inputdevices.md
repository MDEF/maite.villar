---
title: 11. INPUT DEVICES
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

The aim of this week is to to mill a board with an input component. In this documentation I will show information about some of these sensors and the process of milling, soldering and programming to make the board work.

**Input sensors**

-----

How to choose a sensor?
- Sensors and models
- Componenet vs devices
- Sensor interfaces

How to wire a sensor?
- Electronic recipes

How to code for a sensor?
- AnalogRead
- Libraries
- Software serials

**How to measure temperature:**
Temperature sensor gives the information in different always. It can be given in numbers or voltages.
So when it generates a we need to make the relation with the temperature: Example - When the sensor is 0,1V = 10º
The process for this is:

- Calibrated termometre
- Take measurements
- For every V, the Grades are X

![]({{site.baseurl}}/input_temperaturesensor.jpg)

**Milling the board**

-----

In my case I've choosen the **temperature sensor**. For this I've download the files that Nils has in the FabLab page.
- [**Input devices: Temperature sensor (Click here)**](http://academy.cba.mit.edu/classes/input_devices/index.html)

I've downloaded the **traces**, **interior** and the **components file**:

![]({{site.baseurl}}/Hellotemperatureboard.jpg)

The process of milling the board is described in my week 5 *Electronic production* documentation:
- [**Click here to access to the documentation of "milling the board".)**](https://mdef.gitlab.io/maite.villar/fabacademy/electronicsproduction/)


Or more clearly I use to follow this super detailed tutorial for the *SRM20 milling machine*:
- [**For a more detailed tutorial I use this one. Click here.**](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)

![]({{site.baseurl}}/fabmodules_one.png)
![]({{site.baseurl}}/fabmodules_two.png)
![]({{site.baseurl}}/input_temperaturesensor.jpg)

Once the board has been cutted I took the components needed:
![]({{site.baseurl}}/0.jpeg)

and solder it after:
![]({{site.baseurl}}/01.jpeg)

This process starts to be quite easy.

The problems come when trying to program it.

**Programming the board**

---

**Fixing the shortcuts before programming**


I need a ISP connector. It can be 3v or 5v. In this case, my board is 5v. I've connected this to the computer.
From the other side I could use the programmer **AVRISP** or the **Hello board** we cutted the week 5 if it would be working.

![]({{site.baseurl}}/programmingavrps.jpeg)

The light started blinking in orange. That means **the ciurcuit it's not working.**
In two minutes, it started getting super hot and some of the components started melting.

![]({{site.baseurl}}/Fallo1.jpeg)

It's clear that something in the circuit it's going wrong. I fixed the board.

![]({{site.baseurl}}/fixed.jpeg)

I connected it to the computer with the programmer and the ISP connector.
The light doesn't change. It was blinking orange. So I took the multimiter and see if something was wrong again.

The multimiter was beeping when I was touching the PinX and PinY. First I quit the Attiny45 and clean the circuit under it. It still was beeping, so I've noticed that the shortcut could come from the iron under the resistor connecting to that pin. Once I cleaned it it was fixed.

![]({{site.baseurl}}/Inputdevices_fallo1.jpeg)

I've connected again to the programmer and the light was still blinking in orange. Again with the multimiter the connection between other two pins of the chip were biping. Cleaned again the circuit under and still beeping. The 10K resistor next to it was not working. When quiting the resistor the same pins were not beeping, so there was the failure. I've changed the resistor and it worked finally.
![]({{site.baseurl}}/Inputdevices_fallo2_A.jpg)
Time to porgram it.

**Programming the board with the temperature sensor**

First, I've look for the "Software serial library". There is information about this in the Arduino page. [**Arduino page**](https://www.arduino.cc/en/Tutorial/SoftwareSerialExample). I've took the code needed from there.

![]({{site.baseurl}}/input_arduinocode.png)

I've opened the Arduino software and add the code there.
To see wich is the TX and the RX I needed to look at the datasheet of Attiny45.

![]({{site.baseurl}}/input_pinout.jpg)

Looking at the schematic in the same time I've changed in the code:
RX(PB2)2, TX(PB1)1

**SoftwareSerial mySerial(2,1); // RX, TX**

- Change when it says **Serial** to **SoftwareSerial**
- In the window "tools"
- Board: Attiny45
- Processor: Attiny45
- Clock: 1Hz internal. (It's not using any clock so it's not needed to change)
- Programmer: Helloboard
- Burn bootloader

**Change Serial by SoftwareSerial**

The code introduced is next one:
![]({{site.baseurl}}/input_arduino_toolswindow.png)

**Download the files**

---

- Board files [Link to the download the files]({{site.baseurl}}/boardfiles_png.zip)
