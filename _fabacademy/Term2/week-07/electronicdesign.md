---
title: 07. ELECTORNIC DESIGN
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---


## Electronic design
-----

This week the aim is to design a continuous electronic board and programm it. First I'm writting some information that has been usefull to understand the logic behind. I'm going to use a ISP connector, for the communication between the board and the computer. Next the explanation.

**Why do I use a ISP connector?**

- I can access to the microchip without adding extra components.
- I can programm the microchip before soldering
- It is standard. It works with almost all the microchips.
*PIC/AVR/Parallax (Different families of microchips)*

**The ftdi connector**
The digikey microchip is **the one in charge to talk with the usb.**
Topriginal driver to recognize the original microchip is “Ftdi driver”

It's important to know how the cables are connected.
![]({{site.baseurl}}/w7_cable.png)

**Microchip Attiny44**
The microchip affects to what can be connected. In maps like the next, we can see wich are the components that can be connected. In this case is the Atinny 44.

![]({{site.baseurl}}/pin.jpg)

The way the information is sent in this case its called **Synchronous**

**Synchronous protol**
- Just send: Data going from one site to another
- They are not synchronized. The data goes slowly to detect the informacion is being received. Without order
- ISP - Connector with 6 pins


**HOW TO DESIGN YOUR PCB**

-----

Tutorials:
- [**Eagle resources**](http://fab.academany.org/2019/labs/barcelona/local/wa/week6/week06_electronic_design/eagle_resources.md)
- [**FAB EAGLE Library**](https://gitlab.fabcloud.org/pub/projects/blob/master/files/fab.lbr)
- [**Design rules**](https://gitlab.fabcloud.org/pub/projects/blob/master/files/fabmodule.dru)

The first step once the Eagle software is open is to open a Skematic:
**New file > New project > New skematic**

The second step is to download the [**FAB library(Click here)**](http://archive.fabacademy.org/archives/2017/doc/electronics/fab.lbr)

Once the library is download we need to open in in the program:
**Open library manager > Avaiable > fab**

![]({{site.baseurl}}/1_Openlibrary.png)
![]({{site.baseurl}}/2_Librarymanager-fab.png)

For starting designing the board I need to add a design block.
**"> Add design block"**

The components I'm going to use in this board are the next ones:
- ATTINY 44 SSV
- XTAL 20 mHz
- Led
- Button
- Capacitor 22 pf (2)
- AVRIS - 3x2 Pin header

For adding the components follow the next:
**Add part >** *select the component in the Fab Library*

![]({{site.baseurl}}/4_addpart.png)
![]({{site.baseurl}}/3_addattiny.png)

Click on it, place it in the map and create the links. The links are the connections that let the current flow. When doing this we need to take in account the voltage, the ground and the direction mainly.

The links are called “Net”. It appears as a green line in the tools board:

![]({{site.baseurl}}/5_net.png)

For adding a name to the components and the pieces, it’s possible to add a label. This way it possible to identify the parts.

![]({{site.baseurl}}/6_nametools.png)

Once added all the components said at the beginning its needed to change from schematic to the board - **“Generate/Switch to board”**

![]({{site.baseurl}}/7_generateswitchboard.png)
![]({{site.baseurl}}/8_generateswitchboard.png)

Once in that window, the aim is to organize the links in a way that is efficient for the board.

![]({{site.baseurl}}/9_designtheboard.png)

For **saving**, choose only the **top layer.**
The next pictures are my result of the traces and the outline.

![]({{site.baseurl}}/w6_pcbdesign_interior.png)
![]({{site.baseurl}}/w6_pcbdesign_outside.png)

I milled the board using the same process as in week 6 (LINK)-

I added the components by soldering and test if the current was working into the circuit with the multimeter.

![]({{site.baseurl}}/board.png)

The result was that the chip was the way around!! When puting it the way around I quit the copper from the top, so I needed to repeat the board again.

After repeating the milling process, here my new board:

![]({{site.baseurl}}/board2.jpeg)

**Files**

----

- Eagle file: Board [Link to the file]({{site.baseurl}}/resources/w6_board.brd)
- Eagle file: Schematic [Link to the file]({{site.baseurl}}/resources/w6_board.sch)
- Png images: Pcbdesgin_outside [Link to the file]({{site.baseurl}}/resources/w6_pcbdesign_outside.png)
- Png images: Pcbdesgin_interior [Link to the file]({{site.baseurl}}/resources/w6_pcbdesign_interior.png)
