---
title: 17. INTELECTUAL PROPERTY
period: 21-28 January 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

The first task of Fab Academy is to build a documentation website. In Mdef program we have already make our own repository and download another one, duplicate it and use it as template to create our own GitLab page. The content generated is in "Reflections" part of the students websites.

To create a repository un GitLab first I created an account.

To work locally on the repository, generate a SSH key to clone it on your computer, creating a secure connection between the computer and the cloud. To do so:

- On the computer terminal - I am using Hyper - input the commands **ssh-keygen –t rsa –C “$my_email”** and then **cat ~/.ssh/id_rsa.pub** to generate your SSH key

- Authorize the key for the repository by adding it into the Gitlab repository in User Settings » SSH Key

Now that the connection is authorized, it is possible to copy the repository on the computer.

First choose or create the folder where you want your repository to be:

- On the terminal: use the commands cd and ls to move inside the computer.
**cd** enters a directory.
**ls** lists the content of the directory.

Once in the chosen directory, use the command git clone + SSH key to add the Git repository to the directory

We have done this for creating our Mdef's website. In instance, this week I worked in a repository that was not published in my web address, so in these documentation I will resume the task I have being repeating to modify the web, publish the updates and refresh the page and process of duplicating to my web address.


**Looking for my page location**

------

Once you know how to create a repository you should have a Web-page, called as "Project" in GitLab, with a shared repository in the server. If you have started a project in Fab Academy repository you probably have cloned the repository in your terminal and download a software that helps you triggering and building the website.

For the page that in Mdef are running we have downloaded GitBash (Terminal), Atom (https://atom.io/) and Jekyllrb (https://jekyllrb.com/) to run and build the website.

To change the design of the webpage I have duplicated a repository that is working now as a template that I can modify. This repository is not in my official address and I need to look for its address each time I switch on my laptop to see the modifications and in the end of this week change it to the official one.

For that, the first step to do is opening the terminal: Git-bash

![]({{site.baseurl}}/W1_terminal.png)

If you want to start controlling it with Git, you first need to go to that project’s directory.

the command  to go inside a file is:

> $ cd /home/user/my_project

![]({{site.baseurl}}/W1_bash_cd.png)

It's a Master that works as a template. Once inside the file the command to make it work and find the address is:

> $ bundle exec jekyll serve

![]({{site.baseurl}}/W1_bash_adress.png)

Server address: http://127.0.0.1:4000/mdef-jekyll-starter/

![]({{site.baseurl}}/W1_Adress.png)

Already there you can see how your page would be seen. To modify the web we use Atom, there we can edit all our contents before publishing.



**Working in Atom**

------

In Atom in order to work with your repository it's needed to open the "Readme" file that is in the repository file of the laptop.  

The folders are going to affect your web-page. The main file is the project. Inside it there are some files that works for different aims.

**Sass folder** - The most of the variables that conforms the web design. Size, typography, colors, containers, etc its inside this file. The format of these files are .scss

![]({{site.baseurl}}/W1_Saas.png)

**Layouts** - This is what works as a template for the website. Home, page, post. The most complicated layout to design for us is reflection. It works as a list and connects the titles of the pages with it.

![]({{site.baseurl}}/W1_Layouts.png)

**This week's first step to generate a new section in the website dedicated to the Fab Academy has been here.**

1. Duplicate the reflection page. It needs to be in .html format. I have duplicated the reflections page because I wanted the same format to upload the weekly documentation.

2. Second it's needed to copy and paste a .md format document in the project file to make it work. In the following image it can be distinguished as fabacademy.md

![]({{site.baseurl}}/W1_section.png)

Last step to create the new section is to create a folder. With the same name "fab academy" and inside with a folder per week and a .md that works as your page.

Don't forget to add the layout to the page to fix the template.

![]({{site.baseurl}}/W1_fabacademy.png)

The rest of the folders are created in relation to the page contents, the sections, images and contents.



**Steps for uploading files to archive**

------

When creating the repository we made a file in the computer.

![]({{site.baseurl}}/w1_pic0.png)
![]({{site.baseurl}}/w1_pic1.png)

To upload the images and the desing files, I put the files in their respective folder. In my case, the pictures are in the weekly folders, and the design files in a folder called "Resources".

![]({{site.baseurl}}/w1_pic2.png)

Once the file is in the folder I've opened Atom and it shows in the right the files has been uploaded.

![]({{site.baseurl}}/w1_pic3.png)

To introduce the picture I write:

**![]({{site.baseurl}}/w1_picture.png)**

**{site.baseurl}** Gives the reference to the place where the picture is.
**/w1_picture.png** Is the name of the reference with wich you reference it.

Once the text is written and the files are added I clicked **"Stage all"** in the top right, **"commit message"** and **"push"**.

Each time any change is done and is wanted to upload it this steps are necesary:

- (ALWAYS) save/Ctrl S
- Stage All
- write down the name of this modification (so you can reach it later, in case you need to)
- Commit to Master
- Pull

The last action will be **Pull**, so that these modifications can be reflected in the website. These are the COMMAND LINE INSTRUCTIONS to upload the content to the repository in this interface.

This last process takes its time. Something that can help you is to Fetch everything.
