---
title: 09. EMBEDED PROGRAMMING
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

For installing the Windows software and the Drivers I followed this step by step [**documentaiton.**](http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html) **Windows Software / Drivers Install.**

- Get and install avrdude / GCC software and dependencies and drivers:
Warning, WinAVR is abandoned.
*Installing it can destroy your systems path variable!* (Nothing happened to my laptop)
Notes for this issue that I dind't need to trackle *You can use the installer, but before you start, take note of your current system path.*
Download and Install WinAVR - Has a (broken) installer.

**Here is a step-by-step set of instructions**
- After installing check your systems path variable, if it only contains the path to the winavr installation:
- copy those values
- restore your old path
- add the windavr path back to it
- close any commandprompt window you may have open
- Download the drivers for your version of Windows
- Download the FabISP Firmware
- Plug in another FabISP or USBtiny programmer.

Install the drivers:
- Go to the Start menu -> Control Panel -> Device Manager (or run "mmc devmgmt.msc")
- Locate "FabISP" under "Other Devices"
- Right click on the "FabISP"
- Select "Update Driver Software.."
- Select "Browse my computer"
- Navigate to the drivers folder you downloaded at step 4 and click on the folder.
- Hit "ok" to install the drivers for the USBtiny / FabISP

**Powering the board**

I’ve used the mini USB to connect the *Hello world board* to the computer, and the ISP cable to connect the same one to the new board. This way the powers goes through the cables into the 6 pin header.

Once it's the USB diving power to the board and the ISP is connected the light of the AVRISP is going to switch on with a color.

Once connected the light switched on. The color code is the next one:

![]({{site.baseurl}}/1.png)

With the *hello board* the colour was green. So it seems it was working.

**The next step was to edit the Makefile:**
*The Makefile is in the firmware directory that you downloaded. The Makefile is set up to work with the AVRISP2 by default. If you are using another programmer, you will need to edit the Makefile.*

Open the Makefile with [**Notepad++.**](https://notepad-plus-plus.org/)

- **Open your terminal**

cd Desktop/firmware
make clean    
make hex
make fuse
**make program**

In this last command an error appeared:
![]({{site.baseurl}}/error.jpg)

finaly we've solve these connecting to another computer (Mac computer specially).

Now I could step into the process of programming from **Arduino**

**Programming an ATtiny w/ Arduino 1.6 (or 1.0)**

-----

For programming the board I’ve followed the next tutorial:
- [**How to install ATTINY LIBRARIES - GUIDE**](http://highlowtech.org/?p=1695)
- [**Attiny embeded programming**](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html)

**Installing ATtiny support in Arduino 1.6.4**

In Arduino 1.6.4, you can install the ATtiny support using the -built-in boards manager.
- Open the preferences dialog in the Arduino software.
- Find the “Additional Boards Manager URLs” field near the bottom of the dialog

![]({{site.baseurl}}/2_arduino.png)

- Paste the following URL into the field (use a comma to separate it from any URLs you’ve already added):
https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json
- Click the OK button to save your updated preferences.
- Open the boards manager in the “Tools > Board” men
![]({{site.baseurl}}/3_arduino.png)

- Scroll to the bottom of the list; you should see an entry for “ATtiny”.
![]({{site.baseurl}}/4_arduino.png)

Each time you programm a board take in account to change this variables:
**- Boards
- Processor
- Port
- Programmer**

In this case once connected in the arduino IDE software we need to add:
**Board > Attiny/24/44
Processor > Atinny44
Port > miniUSB
Programmer > AVRISP**

This is the board have been programmed to program other boards.
![]({{site.baseurl}}/helloboard.jpeg)

The next step is to add the blink code. It’s in **Examples > Simple > blink**

Add the **pin = 0**, to make the light in the board blink.

![]({{site.baseurl}}/6_arduino.png)

**- Upload**


Next I'm explaining my experience programming the board we have designed to show the learnings of the failures and how I fixed them.

**Contecting the board of week 7**

----

This time I was working with the board made in the week 7 **Electornic design**. For giving power to this board I nedded the 6 pin header and the AVRISP to proggram it.

I program the board with the AVRISP programmer. I connected the AVRISP to the 6 head pin and the tinny usb to the it's entrance.
The light of the programmer started **blinking orange**, that means there's a shortcut.

I took the multimeter and look at the connections to see if something was wrong. In the Arduino software I corrected an error of the reference to the board, but it wasn't still working. So I new that the problem was in the board.

With the datasheet of the Atinny44 I could find that there was missing a connection. In this image it's visible that **SCK** it's not connected.

![]({{site.baseurl}}/8_generateswitchboard.png)

Instead of going by all the process again, I connected a cable from the connection I missed.

![]({{site.baseurl}}/programingtheboard.png)

I connected the programmer and the cable to connect with the 6 pin header. This time the light was blinking green!

For my Attiny the pinout of the led is 7. After I changed the pinMode output in the Arduino program I uploaded it to the board.

![]({{site.baseurl}}/attiny44.jpg)

*The button works letting the current pass and not. So., when we click on it it should affect in switching on and off the LED*
To make this happen I added the next code in the Arduino Software. It's the "Blink" example. I needed to change the OUTPUT with the pin number.

![]({{site.baseurl}}/code.jpg)

A mistake I have in the board is that the Led is not connected. The code is right, the code too but the led it's missing a connection.

In conclusion, I've learn programming a board by programming the helloBoard that after I have used to program other boards of the "input" and "output" assignments. i've larnt how to find the mistakes by using the multimiter and by looking at the connections of the board. The datasheet helped to see with pins are connected and to understand which one is the output pin. The experience programming the week 7's board (the one explained in the end) have been a little bit sad becouse these failures that should be fixed from the beggining in Eagle program. Another possibility is to use the cables not to mill again the board.
