---
title: 05. ELECTRONICS PRODUCTION
period: 13-20 February 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

The aim of this week is to produce our own PCB. For that we had an introduction to electronics and a session to learn how the miller works and how to solder.

I’ve noted the information I think is needed to understand the process I’ve followed to achieve the result and the basics to understand how this works.

To look around the electronic production world, here the link:
  http://academy.cba.mit.edu/classes/electronics_production/index.html


## Bases of electronics
-----

Understanding the physics:
  - Electronic charge of the particles. Positive + negative (together)
  - Positive + positive = Separated
  - Negative negative = Separated

  - Neutron - Atom
  - Proton - Atóm
  - Electron: Floating around the nucleus

Depending the number of this part we have different chemical components:

**Energy** -
Two particles that are together if I want to put them apart I need to put energy and make force.  
This energy is represented as G (Julios).
**Voltage - (Volt)**
The tent that particles that have to come together
Energy / force to came together(Charge) = Voltage.
**Current - (Amperio)**
Jumping electron moving from one atom to the other is the current.
**Resistance - (Ohm’s)**
What makes “things” difficult to move.

*V = IR
Voltage = I(Current) R(Resistance)*

**Components:**

**Resistors -**
The first resistor have less resistance that the second one, in instance, the Led was brighter with the first one. We have measured the voltage with each of them; the first allows to receive 2,15Volts meanwhile the second one only 1,82. The resistance was higher.

![]({{site.baseurl}}/w5_resistor.jpg)

**Capacitor -**
Ceramic capacitor - non polarized
Electrolytic capacitor - (polarized) Unit of capacitor - Farad

  *Function:
  Stabilise voltage
  Store energy
  Super big capacitors works as batteries*

**Diode -**
Current to the same direction


## Electronic production
-----

The aim of this week has been to produce a PCB. The first thing we needed to know for that is to cut it in the milling machine. After cutting, the components need to be soldered in the board. Next I show the process step by step.

Note: For the use of the machine, despite I have no intuition to use it, [**this tutorial**](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
 helped me to make it done. It's perfectly explained.

**Step 1. Traces (1/64)**

-----

Download the files through the [**FabModules**]http://fabmodules.org/().  Note that this tool will detect changes between BLACK and WHITE in the png files and generate a path in it for the machining process.

This is the file I've used for the traces:

![]({{site.baseurl}}/w5_traces.png)

In FabModules pages follow the next steps: (Settings)

- Upload image as *.png*
- Output file as Roland mill (*.rml*)
- PCB traces (1/64)
- Machine: SRM-20
- Origin: X0, Y0 and Z0 are set to 0mm, in order to keep the same origin as the locally defined in the machine
- zjopg - 12
- Speed - 4 or 3 mm/s for new end mills

Click **"calculate"** and after **"save"**.

- Refresh the FabModules page

**Step 2. Outcut (1/32)**

-----

The file I've used for the outcut and holes:

![]({{site.baseurl}}/w5_outcut.png)

In FabModules pages follow the next steps: (Settings)

- Upload image as *.png*
- Output file as Roland mill (*.rml*)
- PCB traces (1/32)
- Machine: SRM-20
- Origin: X0, Y0 and Z0 are set to 0mm, in order to keep the same origin as the locally defined in the machine
- zjopg - 12
- Speed - 1 mm/s for new end mills

*Note: Be aware that Zhome is POSITIVE*

Click **"calculate"** and after **"save"**.

**Step 3. Set up the machine and cut**

-----

Turn on Roland Modela and the computer and open VPanel for SRM-20

![]({{site.baseurl}}/w5_software.jpg)

1. Get a clean board. Be aware it has not fingerprints. *Clean it with alcohol*
2. There is needed a piece of wood. In our case it was there. Be aware it's plane.
3. Stick *two sides* tape in the back part of the board.
4. Stick the board in the piece of wood.
*The first time I tried to cut the board it was not good stick to the board. In that case you loose a piece of the board and need to do it again. Stick it properly, it's important.*

Cutting the traces (1/64)

-----

The next step is to set up the (0,0) point of the model you want to cut.
*The (0,0) of your file created in fab modules is in the bottom left corner.*
The mill I have used for this is 1/64.

1. Move the machine using the X/Y arrows. When using Z arrow be careful with the speed.
For that change the *Cursor step*: From *continuous* to *100*, *10* or *1*.
2. Move it to the bottom left corner.
3. Set the Z(0)
  - Move the Z above the plate. Be careful not to touch the board while moving it.
  - One 2-3 mm above you need to fix it manualy.
  - With a screw wrench in a hand hold the endmill with the other. In the top of the mill you will see a screw that     holds it. It has a holl. Open it wich the wrench and hold the endmill not to BREAK when opening.
  - *Press “slightly down” the PCB board, around 0.5mm and tight the endmill with the screw on the collet.*
4. Save the X/Y and the Z in the software panel.

Now CLICK **CUT**.
1. Upload the **Traces** file has been done in FabModules.
2. Click **Output**

Once this has been finished the next step is to cut the outside.

Cutting the outcut (1/32)

-----

To make this done I needed to change the mill. (1/32)
The machine is already set up so I've only needed to change the mill and set the Z(0) as before.

1. Change the mill with the screw wrench.
2. Set the Z(0)
  - Move the Z above the plate. Be careful not to touch the board while moving it.
  - One 2-3 mm above you need to fix it manualy.
  - With a screw wrench in a hand hold the endmill with the other. In the top of the mill you will see a screw that     holds it. It has a holl. Open it wich the wrench and hold the endmill not to BREAK when opening.
  - *Press “slightly down” the PCB board, around 0.5mm and tight the endmill with the screw on the collet.*
3. Save the Z in the software panel.

Repeat the process to send it to the machine.
CLICK **CUT**.
1. Upload the **Outcut** file has been done in FabModules.
2. Click **Output**

**Let the magic get done**

![]({{site.baseurl}}/w5_video.gif)

**Soldering the components**

-----

Look for the components needed in you PCB. Before soldering the components the suggestion was to put all the components in a paper and write the reference in a side.

FOTO DE LA HOJA CON LOS COMPONENTES

- The tools needed for soldering are these: The solderer, the solder, a sponge, and tweezers.
- Tips: Putting more soldier helps a lot. Use the copper to quit it.

**Soldering the pieces in the PCB**

This maps shows how to solder the components:
![]({{site.baseurl}}/mapa.png)

These are the all the parts you need to build the board.

- 1 ATTiny 44 microcontroller
- 1 Capacitor 1uF
- 2 Capacitor 10 pF
- 2 Resistor 100 ohm
- 1 Resistor 499 ohm
- 1 Resistor 1K ohm
- 1 Resistor 10K
- one 6 pin header
- 1 USB connector
- 2 jumpers - 0 ohm resistors
- 1 Crystal 20MHz
- two Zener Diode 3.3 V
- one usb mini cable
- one ribbon cable
- two 6 pin connectors

Material for soldering and steps:
![]({{site.baseurl}}/w5_solding.jpeg)

1. Put a little bit of solder in one of the legs. *Don’t put the solder in top of the iron*
2. Stick a leg. First put the soldier in the ground and add more sold when sticking the piece to it.Once one leg is fixed, stick another one. *If you see all the board is getting brown / is burning, it means that the temperature used its too much.*
3. Ex. Soldering the led: The LED has a green slide, it’s the reference to know its orientation. In the place where the led goes in the PCB there are two lines, one of them is fatter than the other. The green side goes on the top of the fatter line.

![]({{site.baseurl}}/w5_result.jpeg)
