---
title: 06. 3D SCANNING AND PRINTING
period: 21-26 February 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

This week under the title "molding and casting" our assignment was to make a 3D print and scan something in 3D.
[**Here the link**](http://academy.cba.mit.edu/classes/molding_casting/index.html) to access to this week's information in FabAcademy's oficial page.

## 3D printing
-----

The content you can read next on the one hand is the features of 3d printers and how do they differ? And on the other, the set up of one of them.



**Set up the printer and print something**

-----
For the assigment of this week I have 3D printed a bracelet. As introductory experience for 3d printing it has been helpfull to understand the process of preparing a file and sending it to the machine.

Next the steps given for 3d printing:

**First**, prepare the file.
- Prepare the file so that you know that for the printer it will be easier to print.*
- Save the file in .xtl
- Note: If there is more than one object to save each in a file.

**Second**, Set up the machine
- Select the printer (Switch it on manually)
*In my case the printer I’ve used is called* “[**PRUSA**](https://www.prusa3d.es/original-prusa-i3-mk3-spa/ )”*

FOTO THE LA IMPRESORA

- Open the software of the printer in the computer
![]({{site.baseurl}}/Fotoapantalla1.png)

- Import the file. *Files - Import*
- Click the solid view to see if everything is okay in the design
- If the pieces are not okey edit the file again
- Edit the "printer setting"
  When adding wich materials to use automatically it defines the next parameters.
  Quality, fill and support.
  *In my case I needed to put a support that meant a lot of plastic use.In general is better to change the original design. In this case, the object is small so I went ahead.*
- Once clicked “Generate” it gives the option to modify the “Object settings”.
 *I used the “Rotation” to rotate around the bracelet and make it touch more surface in order take advantage of the Z0 plane to start building from.*
- It’s important to take in account the object’s characteristics to understand if it’s more friendly for the printer or not.
- Once the file prepared in the software I saved it in a SD card.
- Take out the sd card and put it into the printer.
- Select in the screen: Print from SD card
- PLAY!.

In the screen are visible the printing setting. The hit and how its getting hoter, etc.
FOTO DE LA PANTALLA

![]({{site.baseurl}}/3dprinting.gif)

The next is the result:

![]({{site.baseurl}}/w5_result1.jpeg)


This image is the result of an downloaded file. I made it with the aim to understand a little the 3d printer and its software. Once made this excersice, I have designed the next bracelet in Rhinoceros. It's done specially to 3d print it. It can't be done substactevly because with three axes it couldn't access to cut the empty spaces on it. In the case of 3D printing it can build support lines to build it and remove them after.

First I've measured my hand side to side to make a holl in the bracelet that could fix in my arm.
I've started with a circle based in the measurment of my wrist.
![]({{site.baseurl}}/rhino_1.png)

I completed the geometry making surfaces between two lines one next to the other and extruding them.
![]({{site.baseurl}}/rhino_2.png)

I have exported the selected section as **.stl** and save in in a pendrvie.

This time I've used the **Ultimaker 3d printer.**
![]({{site.baseurl}}/ultimaker.jpeg)
To print in this machine I've opened the **.stl** file in **CURA**.
![]({{site.baseurl}}/ultimaker_software.jpeg)
Next, I've adapted the variables to the object I was going to print. It needed supports everywhere but not a lot to remove them after. Next save
![]({{site.baseurl}}/ultimaker_prin1.jpeg)

- In [**THIS PAGE**](https://ultimaker.com/en/resources/21932-mastering-cura) you can find the characteristics of the variables to work with in the software.

After saving it in a SD card, I've put it in the machine and print it. **Note: Before printing put "Laca" in the surface of the 3D printer to stick the material to it.** > Play!

![]({{site.baseurl}}/brazalete_gif.mp4)
![]({{site.baseurl}}/brazalete.jpeg)


**Download design files:**

---

- Rhinoceros file: .3dm format [Link to the file]({{site.baseurl}}/3d_model_bracelet.3dm)
- .stl file: Bracelet [Link to the file]({{site.baseurl}}/3d_bracelet.stl)


## Scanning in 3D
-----

Scanned an Object
Outlined problems and how you fixed them
Included your design files and "hero shortcut" photos of the scan and the final object.

**Open SKANECT.**
newScene: Object
Bounding box: 1.5x1.5 meters
Aspect ratio: Normal
Path: Desktop
Config files: none
START.

The camera is not working -
Off. Pic1.

We took another camera. It had an american plug, so we have change it to an spanish plug.

We put the same parameters, click Start and it's still not working.
sensor unavailable , so I checked the kinect usb port on the computer and nothing abnormal is happening. I disconnected and reconnected the camera, nothing happens. What to do? Google search for Kine ct not responding. Google suggested to try with different usb port. So we looked for another usb port with another Kinect. Oh¡ there's a bip¡ it sounds like when my battery needs to get charged. I think maybe the usb has been detected.
Ah¡ Gustavo saved us and and showed that we simply needed to plug the usb BEFORE we open the Skanect software. And here it starts¡
oh no again¡ Skanect tells us it did not find a source of data We went to check the full  instructions online and went back to google

Gustavo let us his Mac book and ther it worked properly.
Click START and walk around in a distance to capture the Object.

-timelapse / Julia walkin aroung
-Timelapse / Computer recording

We needed to record it two times because in the first trial we went out of the scope.

In the settings we've played filling the image captured and this is the result we get.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/faf87bd27eac4f23aa0264d86905408a/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
<a href="https://sketchfab.com/3d-models/chair-faf87bd27eac4f23aa0264d86905408a?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">chair</a> by <a href="https://sketchfab.com/gustavo-deabreu?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Gustavo Abreu</a> on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>
